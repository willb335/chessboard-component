import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Chess from 'chess.js';

const game = new Chess();

class HumanVsHuman extends Component {
  static propTypes = {
    children: PropTypes.func
  };

  state = { fen: 'start', highLightedSquares: [] };

  removeHighlightSquare = () => {
    this.setState({ highLightedSquares: [] });
  };

  highlightSquare = (sourceSquare, squares = []) => {
    this.setState(() => ({
      highLightedSquares: [sourceSquare, ...squares]
    }));
  };

  onDrop = (source, target) => {
    this.removeHighlightSquare();

    // see if the move is legal
    var move = game.move({
      from: source,
      to: target,
      promotion: 'q' // NOTE: always promote to a queen for example simplicity
    });

    // illegal move
    if (move === null) return;

    this.setState({ fen: game.fen() });
  };

  onMouseOverSquare = square => {
    // get list of possible moves for this square
    var moves = game.moves({
      square: square,
      verbose: true
    });

    // exit if there are no moves available for this square
    if (moves.length === 0) return;

    // highlight the square they moused over
    // highlight the possible squares for this piece
    let squaresToHighlight = [];
    for (var i = 0; i < moves.length; i++) {
      squaresToHighlight.push(moves[i].to);
    }
    this.highlightSquare(square, squaresToHighlight);
  };

  onMouseOutSquare = square => {
    this.removeHighlightSquare();
  };

  render() {
    const { fen, highLightedSquares } = this.state;
    return this.props.children({
      position: fen,
      highLightedSquares,
      onMouseOverSquare: this.onMouseOverSquare,
      onMouseOutSquare: this.onMouseOutSquare,
      onDrop: this.onDrop
    });
  }
}

export default HumanVsHuman;
