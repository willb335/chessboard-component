import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Chess from 'chess.js';

const game = new Chess();

class RandomEngine extends Component {
  static propTypes = {
    children: PropTypes.func
  };

  state = { fen: 'start' };

  componentDidMount() {
    this.makeRandomMove();
  }

  componentWillUnmount() {
    window.clearTimeout(this.timer());
  }

  timer = () => window.setTimeout(this.makeRandomMove, 750);

  makeRandomMove = () => {
    let possibleMoves = game.moves();

    // exit if the game is over
    if (
      game.game_over() === true ||
      game.in_draw() === true ||
      possibleMoves.length === 0
    )
      return;

    let randomIndex = Math.floor(Math.random() * possibleMoves.length);
    game.move(possibleMoves[randomIndex]);
    this.setState({ fen: game.fen() });

    this.timer();
  };

  render() {
    const { fen } = this.state;
    return this.props.children({
      position: fen
    });
  }
}

export default RandomEngine;
