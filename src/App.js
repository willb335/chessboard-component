import React, { Component } from 'react';
// import HTML5Backend from 'react-dnd-html5-backend';
// import { DragDropContext } from 'react-dnd';

import Demo from './Demo';

class App extends Component {
  render() {
    return <Demo />;
  }
}

// export default DragDropContext(HTML5Backend)(App);
export default App;
