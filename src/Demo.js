import React, { Component } from 'react';

import Chessboard from './Chessboard';
import RandomEngine from './RandomEngine';
import Engine from './Engine';
import HumanVsHuman from './HumanVsHuman';
import { customizeSquare } from './custom';

class Demo extends Component {
  render() {
    return (
      <div style={mainContainer}>
        <div style={title}>Chessboard.jsx</div>
        <div style={boardsContainer}>
          <div>
            <div style={board}>
              <HumanVsHuman>
                {({
                  position,
                  highLightedSquares,
                  onDrop,
                  onMouseOverSquare,
                  onMouseOutSquare
                }) => (
                  <Chessboard
                    id="standard"
                    width={350}
                    position={position}
                    highLightedSquares={highLightedSquares}
                    onDrop={onDrop}
                    onMouseOverSquare={onMouseOverSquare}
                    onMouseOutSquare={onMouseOutSquare}
                  />
                )}
              </HumanVsHuman>
            </div>
            <div style={boardDescriptions}>With Move Validation</div>
          </div>
          <div>
            <div style={board}>
              <Chessboard
                id="ruyLopez"
                width={350}
                position="r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R"
                orientation="white"
                sparePieces={true}
              />
            </div>
            <div style={boardDescriptions}>With Spare Pieces</div>
          </div>
          <div>
            <div style={board}>
              <Chessboard
                width={350}
                id="frozen"
                position={{
                  a4: 'bK',
                  c4: 'wK',
                  a7: 'wR'
                }}
                draggable={false}
              />
            </div>
            <div style={boardDescriptions}>Not Draggable</div>
          </div>
          <div>
            <div style={board}>
              <RandomEngine>
                {({ position }) => (
                  <Chessboard
                    width={350}
                    id="random"
                    orientation="black"
                    position={position}
                    draggable={false}
                  />
                )}
              </RandomEngine>
            </div>
            <div>
              <div style={boardDescriptions}>Random v Random</div>
            </div>
          </div>
          <div>
            <div style={board}>
              <Engine>
                {({ position, onDrop }) => (
                  <Chessboard
                    id="stockfish"
                    position={position}
                    width={350}
                    onDrop={onDrop}
                    // imageFormat="png"
                  />
                )}
              </Engine>
            </div>
            <div style={boardDescriptions}>Play Stockfish</div>
          </div>
          <div>
            <div style={board}>
              <Chessboard
                id="custom"
                width={350}
                animationOnDrop="rubberBand"
                customizeSquare={customizeSquare}
                whiteSquareStyle={{ backgroundColor: '#b58863' }}
                blackSquareStyle={{ backgroundColor: '#f0d9b5' }}
                position={{
                  d6: 'bK',
                  d4: 'wP',
                  e4: 'wK'
                }}
              />
            </div>
            <div style={boardDescriptions}>Custom</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Demo;

const boardsContainer = {
  display: 'flex',
  justifyContent: 'space-around',
  alignItems: 'center',
  flexWrap: 'wrap',
  width: '90vw',
  fontFamily: 'Neucha, cursive'
};

const title = {
  fontFamily: 'Neucha, cursive',
  fontSize: '5rem',
  margin: 15,
  position: 'relative',
  display: 'flex',
  width: 'auto'
};

const mainContainer = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  width: '100vw'
};

const boardDescriptions = {
  fontSize: '2rem',
  textAlign: 'center'
};

const board = { margin: 30 };
