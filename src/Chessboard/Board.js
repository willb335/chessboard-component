import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Piece from './Piece';
import Square from './Square';
import Notation from './Notation';
import { COLUMNS } from './helpers';
import Chessboard from './index';
import PhantomPiece from './PhantomPiece';

const showPhantom = (currentSquare, targetSquare, phantomPiece) => {
  const isActivePiece = (currentSquare, targetSquare) =>
    targetSquare && targetSquare === currentSquare;

  return phantomPiece && isActivePiece(currentSquare, targetSquare);
};

class Board extends Component {
  static propTypes = {
    orientation: PropTypes.oneOf(['white', 'black']),
    currentPosition: PropTypes.object,
    width: PropTypes.number,
    showNotation: PropTypes.bool,
    setPosition: PropTypes.func,
    onDrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    whiteSquareStyle: PropTypes.object,
    blackSquareStyle: PropTypes.object,
    boardStyle: PropTypes.object,
    customizeSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    highLightedSquares: PropTypes.array,
    onMouseOverSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onMouseOutSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onHoverSquareStyle: PropTypes.object,
    highLightedSquareStyleWhite: PropTypes.object,
    highLightedSquareStyleBlack: PropTypes.object
  };

  setSquareCoordinates = (x, y, square) =>
    this.setState({ [square]: { x, y } });

  getSquareCoordinates = (sourceSquare, targetSquare) => ({
    sourceSquare: this.state[sourceSquare],
    targetSquare: this.state[targetSquare]
  });

  render() {
    const {
      orientation,
      currentPosition,
      width,
      showNotation,
      setPosition,
      onDrop,
      whiteSquareStyle,
      blackSquareStyle,
      boardStyle,
      customizeSquare,
      highLightedSquares,
      onMouseOverSquare,
      onMouseOutSquare,
      onHoverSquareStyle,
      highLightedSquareStyleWhite,
      highLightedSquareStyleBlack
    } = this.props;

    let alpha = COLUMNS;
    let row = 8;
    let squareColor = 'white';

    if (orientation === 'black') row = 1;

    return (
      <div
        style={{ ...boardStyles(width), ...boardStyle }}
        className={cx('board')}
      >
        {[...Array(8)].map((_, r) => {
          row = orientation === 'black' ? row + 1 : row - 1;

          return (
            <div key={r.toString()} className={'row'}>
              {[...Array(8)].map((_, col) => {
                let square =
                  orientation === 'black'
                    ? alpha[7 - col] + (row - 1)
                    : alpha[col] + (row + 1);

                if (col !== 0)
                  squareColor = squareColor === 'black' ? 'white' : 'black';

                return (
                  <Square
                    key={col.toString()}
                    width={width}
                    square={square}
                    squareColor={squareColor}
                    setSquareCoordinates={this.setSquareCoordinates}
                    whiteSquareStyle={whiteSquareStyle}
                    blackSquareStyle={blackSquareStyle}
                    customizeSquare={customizeSquare}
                    highLightedSquares={highLightedSquares}
                    onMouseOverSquare={onMouseOverSquare}
                    onMouseOutSquare={onMouseOutSquare}
                    onHoverSquareStyle={onHoverSquareStyle}
                    highLightedSquareStyleWhite={highLightedSquareStyleWhite}
                    highLightedSquareStyleBlack={highLightedSquareStyleBlack}
                  >
                    {currentPosition &&
                      Object.keys(currentPosition) &&
                      Object.keys(currentPosition).includes(square) && (
                        <Chessboard.Consumer>
                          {({
                            imageFormat,
                            dropOffBoard,
                            draggable,
                            sourceSquare,
                            targetSquare,
                            animationOnDrop,
                            pieceSvgs,
                            waitForTransition,
                            phantomPiece,
                            transitionDuration,
                            piecePngs,
                            orientation,
                            id,
                            beginAnimation,
                            dropSquare,
                            setAnimation,
                            setTransition,
                            setTouchState
                          }) => (
                            <Fragment>
                              {showPhantom(
                                square,
                                targetSquare,
                                phantomPiece
                              ) && (
                                <PhantomPiece
                                  width={width}
                                  phantomPieceValue={phantomPiece[targetSquare]}
                                  pieceSvgs={pieceSvgs}
                                  piecePngs={piecePngs}
                                  imageFormat={imageFormat}
                                />
                              )}
                              <Piece
                                pieceSvgs={pieceSvgs}
                                piecePngs={piecePngs}
                                currentSquare={square}
                                piece={currentPosition[square]}
                                width={width}
                                imageFormat={imageFormat}
                                setPosition={setPosition}
                                dropOffBoard={dropOffBoard}
                                getSquareCoordinates={this.getSquareCoordinates}
                                draggable={draggable}
                                onDrop={onDrop}
                                sourceSquare={sourceSquare}
                                targetSquare={targetSquare}
                                animationOnDrop={animationOnDrop}
                                waitForTransition={waitForTransition}
                                phantomPiece={phantomPiece}
                                transitionDuration={transitionDuration}
                                orientation={orientation}
                                beginAnimation={beginAnimation}
                                dropSquare={dropSquare}
                                id={id}
                                setAnimation={setAnimation}
                                setTransition={setTransition}
                                setTouchState={setTouchState}
                              />
                            </Fragment>
                          )}
                        </Chessboard.Consumer>
                      )}
                    {showNotation && (
                      <Notation
                        row={row}
                        col={col}
                        alpha={alpha}
                        orientation={orientation}
                        width={width}
                        whiteSquareStyle={whiteSquareStyle}
                        blackSquareStyle={blackSquareStyle}
                      />
                    )}
                  </Square>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

export default Board;

const boardStyles = width => ({
  width: width,
  height: width,
  position: 'relative',
  boxSizing: 'content-box',
  cursor: 'default'
});
