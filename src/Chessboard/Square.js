import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';
import cx from 'classnames';

import { ItemTypes } from './helpers';

class Square extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func,
    width: PropTypes.number,
    squareColor: PropTypes.oneOf(['white', 'black']),
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.node]),
    isOver: PropTypes.bool,
    square: PropTypes.string,
    setSquareCoordinates: PropTypes.func,
    whiteSquareStyle: PropTypes.object,
    blackSquareStyle: PropTypes.object,
    customizeSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    highLightedSquares: PropTypes.array,
    onMouseOverSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onMouseOutSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onHoverSquareStyle: PropTypes.object,
    highLightedSquareStyleWhite: PropTypes.object,
    highLightedSquareStyleBlack: PropTypes.object
  };

  componentDidMount() {
    const { square, setSquareCoordinates, width, customizeSquare } = this.props;
    customizeSquare && customizeSquare(this.squareSvg, width / 8);

    const { x, y } = this[square].getBoundingClientRect();
    setSquareCoordinates(x, y, square);
  }

  render() {
    const {
      connectDropTarget,
      width,
      squareColor,
      children,
      isOver,
      square,
      whiteSquareStyle,
      blackSquareStyle,
      customizeSquare,
      highLightedSquares,
      onMouseOverSquare,
      onMouseOutSquare,
      onHoverSquareStyle,
      highLightedSquareStyleWhite,
      highLightedSquareStyleBlack
    } = this.props;

    return connectDropTarget(
      <div
        data-testid={`${squareColor}-square`}
        ref={ref => (this[square] = ref)}
        className={cx('square')}
        style={squareStyles({
          width,
          squareColor,
          isOver,
          blackSquareStyle,
          whiteSquareStyle,
          highLightedSquares,
          square,
          onHoverSquareStyle,
          highLightedSquareStyleWhite,
          highLightedSquareStyleBlack
        })}
        onMouseOver={() => onMouseOverSquare && onMouseOverSquare(square)}
        onMouseOut={() => onMouseOutSquare && onMouseOutSquare(square)}
      >
        {customizeSquare ? (
          <Fragment>
            <svg
              ref={ref => (this.squareSvg = ref)}
              width={width / 8}
              height={width / 8}
              style={{ display: 'block' }}
            />
            <div style={customSquareStyles}>{children}</div>
          </Fragment>
        ) : (
          children
        )}
      </div>
    );
  }
}

const squareTarget = {
  drop(props) {
    return { target: props.square };
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

export default DropTarget(ItemTypes.PIECE, squareTarget, collect)(Square);

const squareStyles = ({
  width,
  squareColor,
  isOver,
  blackSquareStyle,
  whiteSquareStyle,
  highLightedSquares,
  square,
  onHoverSquareStyle,
  highLightedSquareStyleWhite,
  highLightedSquareStyleBlack
}) => {
  const getHighlightColor = () => {
    if (highLightedSquares.length && highLightedSquares.includes(square)) {
      return squareColor === 'white'
        ? highLightedSquareStyleWhite
        : highLightedSquareStyleBlack;
    }
  };

  return {
    ...{
      width: width / 8,
      height: width / 8,

      ...(squareColor === 'black' ? blackSquareStyle : whiteSquareStyle),
      ...getHighlightColor(),
      ...(isOver && onHoverSquareStyle)
    }
  };
};

const customSquareStyles = {
  position: 'absolute',
  zIndex: 2,
  height: '100%',
  width: '100%'
};
