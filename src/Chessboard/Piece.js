import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import PropTypes from 'prop-types';
import { getEmptyImage } from 'react-dnd-html5-backend';
import cx from 'classnames';

import { ItemTypes } from './helpers';
import Svg from './Svg';
import Png from './Png';
import './styles/pieceAnimations.css';

class Piece extends Component {
  static propTypes = {
    piece: PropTypes.string,
    currentSquare: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.number,
    connectDragSource: PropTypes.func,
    imageFormat: PropTypes.oneOf(['svg', 'png']),
    isDragging: PropTypes.bool,
    connectDragPreview: PropTypes.func,
    dropOffBoard: PropTypes.string,
    getSquareCoordinates: PropTypes.func,
    onDrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    animationOnDrop: PropTypes.string,
    transitionDuration: PropTypes.number,
    pieceSvgs: PropTypes.objectOf(PropTypes.object),
    sourceSquare: PropTypes.string,
    targetSquare: PropTypes.string,
    waitForTransition: PropTypes.bool,
    phantomPiece: PropTypes.object,
    piecePngs: PropTypes.string,
    beginAnimation: PropTypes.bool,
    dropSquare: PropTypes.string,
    setTouchState: PropTypes.func
  };

  componentDidMount() {
    window.addEventListener('touchstart', this.props.setTouchState);

    this.props.connectDragPreview(getEmptyImage(), {
      captureDraggingState: true
    });
  }

  componentWillUnmount() {
    document.removeEventListener('touchstart', this.props.setTouchState);
  }

  render() {
    const {
      piece,
      width,
      connectDragSource,
      imageFormat,
      currentSquare,
      isDragging,
      getSquareCoordinates,
      animationOnDrop,
      pieceSvgs,
      sourceSquare,
      targetSquare,
      waitForTransition,
      phantomPiece,
      transitionDuration,
      piecePngs,
      beginAnimation,
      dropSquare
    } = this.props;

    return connectDragSource(
      <div
        className={cx(
          { grab: !isDragging },
          { move: isDragging },
          {
            [animationOnDrop]: beginAnimation && currentSquare === dropSquare
          }
        )}
      >
        {imageFormat === 'png' ? (
          <Png
            piecePngs={piecePngs}
            currentSquare={currentSquare}
            width={width}
            piece={piece}
            isDragging={isDragging}
            getSquareCoordinates={getSquareCoordinates}
            sourceSquare={sourceSquare}
            targetSquare={targetSquare}
            waitForTransition={waitForTransition}
            phantomPiece={phantomPiece}
            transitionDuration={transitionDuration}
          />
        ) : (
          <Svg
            pieceSvgs={pieceSvgs}
            currentSquare={currentSquare}
            width={width}
            piece={piece}
            isDragging={isDragging}
            getSquareCoordinates={getSquareCoordinates}
            sourceSquare={sourceSquare}
            targetSquare={targetSquare}
            waitForTransition={waitForTransition}
            phantomPiece={phantomPiece}
            transitionDuration={transitionDuration}
          />
        )}
      </div>
    );
  }
}

const pieceSource = {
  canDrag(props) {
    return props.draggable;
  },
  beginDrag(props) {
    return {
      piece: props.piece,
      sourceSquare: props.currentSquare,
      board: props.id
    };
  },
  endDrag(props, monitor) {
    const dropResults = monitor.getDropResult();
    const didDrop = monitor.didDrop();

    if (!didDrop && props.dropOffBoard === 'trash') {
      props.setPosition(props.piece, props.currentSquare);
      return;
    }

    if (didDrop) {
      if (props.onDrop) {
        props.setTransition(false);
        props.onDrop(props.currentSquare, dropResults.target);
        props.setAnimation(dropResults.target);
        return;
      }
      props.setPosition(props.piece, props.currentSquare, dropResults.target);
      props.setAnimation(dropResults.target);
      return;
    }
  }
};
function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
    dropTarget: monitor.getDropResult()
  };
}

export default DragSource(ItemTypes.PIECE, pieceSource, collect)(Piece);
