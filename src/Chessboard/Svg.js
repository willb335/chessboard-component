import React, { Component } from 'react';
import PropTypes from 'prop-types';

const isActivePiece = (currentSquare, targetSquare) =>
  targetSquare && targetSquare === currentSquare;

const getTransitionCoordinates = ({
  getSquareCoordinates,
  sourceSq,
  targetSq
}) => {
  const transitionCoordinates = getSquareCoordinates(sourceSq, targetSq);
  const { sourceSquare, targetSquare } = transitionCoordinates;

  return `translate(${sourceSquare.x - targetSquare.x}px, ${sourceSquare.y -
    targetSquare.y}px)`;
};

class Svg extends Component {
  static propTypes = {
    width: PropTypes.number,
    piece: PropTypes.string,
    isDragging: PropTypes.bool,
    currentSquare: PropTypes.string,
    getSquareCoordinates: PropTypes.func,
    pieceSvgs: PropTypes.object,
    sourceSquare: PropTypes.string,
    targetSquare: PropTypes.string,
    waitForTransition: PropTypes.bool,
    transitionDuration: PropTypes.number
  };

  getTransitionCoordinates = () => {
    const {
      waitForTransition,
      currentSquare,
      targetSquare,
      sourceSquare,
      getSquareCoordinates
    } = this.props;

    return (
      waitForTransition &&
      isActivePiece(currentSquare, targetSquare) &&
      getTransitionCoordinates({
        getSquareCoordinates,
        sourceSq: sourceSquare,
        targetSq: targetSquare
      })
    );
  };

  render() {
    const {
      width,
      piece,
      isDragging,
      pieceSvgs,
      transitionDuration
    } = this.props;

    return pieceSvgs ? (
      <svg
        viewBox={`-3 -3 50 50`}
        style={{
          width: width / 8,
          height: width / 8,
          opacity: isDragging ? 0 : 1,
          transform: this.getTransitionCoordinates(),
          transition: `transform ${transitionDuration}ms`,
          position: 'absolute',
          zIndex: 10
        }}
      >
        {Object.keys(pieceSvgs).map(key => {
          if (key !== piece) return null;
          return (
            <g data-testid={key} key={key}>
              {pieceSvgs[key]}
            </g>
          );
        })}
      </svg>
    ) : null;
  }
}

export default Svg;
