import React, { Component } from 'react';
import Board from './Board';
import PropTypes from 'prop-types';
import isEqual from 'lodash.isequal';
import { DragDropContext } from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch';

import { SparePiecesTop, SparePiecesBottom } from './SparePieces';
import {
  fenToObj,
  validFen,
  validPositionObject,
  constructPositionAttributes
} from './helpers';
import './styles/chessboard.css';
import CustomDragLayer from './CustomDragLayer';
import standard from './svg/chesspieces/standard';

const pngPathForPieces = './img/chesspieces/wikipedia/';
const ChessboardContext = React.createContext();

const getPosition = position => {
  if (position === 'start')
    return fenToObj('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR');
  if (validFen(position)) return fenToObj(position);
  if (validPositionObject(position)) return position;

  return {};
};

class Chessboard extends Component {
  static propTypes = {
    position: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    orientation: PropTypes.oneOf(['white', 'black']),
    sparePieces: PropTypes.bool,
    width: PropTypes.number,
    imageFormat: PropTypes.oneOf(['svg', 'png']),
    pieceSvgs: PropTypes.objectOf(PropTypes.object),
    piecePngs: PropTypes.string,
    dropOffBoard: PropTypes.string,
    animationOnDrop: PropTypes.string,
    onDrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    draggable: PropTypes.bool,
    transitionDuration: PropTypes.number,
    showNotation: PropTypes.bool,
    whiteSquareStyle: PropTypes.object,
    blackSquareStyle: PropTypes.object,
    boardStyle: PropTypes.object,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    customizeSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    highLightedSquares: PropTypes.array,
    onMouseOverSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onMouseOutSquare: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    onHoverSquareStyle: PropTypes.object,
    highLightedSquareStyleWhite: PropTypes.object,
    highLightedSquareStyleBlack: PropTypes.object
  };

  static defaultProps = {
    width: 650,
    orientation: 'white',
    showNotation: true,
    position: '',
    sparePieces: false,
    draggable: true,
    dropOffBoard: 'trash',
    imageFormat: 'svg',
    pieceSvgs: standard,
    piecePngs: pngPathForPieces,
    animationOnDrop: 'pulse',
    onDrop: false,
    transitionDuration: 300,
    whiteSquareStyle: { backgroundColor: '#61dafb' },
    blackSquareStyle: { backgroundColor: '#10a8c8' },
    boardStyle: {
      borderRadius: '5px',
      boxShadow: `0 5px 15px rgba(0, 0, 0, 0.5)`
    },
    onHoverSquareStyle: { boxShadow: `inset 0 0 1px 3px yellow` },
    highLightedSquareStyleWhite: { backgroundColor: `gainsboro` },
    highLightedSquareStyleBlack: { backgroundColor: `lightslategrey` },
    id: '0',
    customizeSquare: false,
    highLightedSquares: [],
    onMouseOverSquare: false,
    onMouseOutSquare: false
  };

  static Consumer = ChessboardContext.Consumer;

  state = {
    currentPosition: getPosition(this.props.position),
    sourceSquare: '',
    targetSquare: '',
    sourcePiece: '',
    waitForTransition: false,
    phantomPiece: null,
    beginAnimation: false,
    dropSquare: null,
    transition: false,
    wasPieceTouched: false
  };

  componentDidUpdate(prevProps) {
    const { position, transitionDuration } = this.props;
    const { transition } = this.state;
    if (transition && prevProps.position !== position) {
      this.setPositionPromise(position, transitionDuration).then(() =>
        setTimeout(
          () => this.setState({ phantomPiece: null }),
          transitionDuration
        )
      );
      return;
    }
    if (!transition && prevProps.position !== position)
      this.setState({ transition: true });
  }

  setPositionPromise = (position, transitionDuration) => {
    return new Promise(resolve => {
      this.setState({ currentPosition: fenToObj(position) }, () =>
        setTimeout(() => {
          this.setState({ waitForTransition: false });
          resolve();
        }, transitionDuration)
      );
    });
  };

  static getDerivedStateFromProps(props, state) {
    const { position } = props;
    const { currentPosition, transition } = state;
    const newPos = fenToObj(position);

    if (isEqual(newPos, currentPosition)) return null;

    const {
      sourceSquare,
      targetSquare,
      sourcePiece
    } = constructPositionAttributes(currentPosition, fenToObj(position));

    if (transition) {
      if (currentPosition[targetSquare]) {
        delete newPos[targetSquare];

        return {
          sourceSquare,
          targetSquare,
          sourcePiece,
          currentPosition: newPos,
          waitForTransition: true,
          phantomPiece: { [targetSquare]: state.currentPosition[targetSquare] },
          beginAnimation: false,
          dropSquare: null
        };
      }

      return {
        sourceSquare,
        targetSquare,
        sourcePiece,
        currentPosition: getPosition(position),
        waitForTransition: true,
        beginAnimation: false,
        dropSquare: null
      };
    }

    return {
      currentPosition: getPosition(position),
      sourceSquare,
      targetSquare,
      sourcePiece,
      beginAnimation: false,
      dropSquare: null
    };
  }

  setAnimation = targetSquare =>
    this.setState({ beginAnimation: true, dropSquare: targetSquare });

  setTransition = bool => this.setState({ transition: bool });

  setPosition = (piece, sourceSquare, targetSquare = null) => {
    if (sourceSquare === targetSquare) return;

    if (this.props.dropOffBoard === 'trash' && !targetSquare) {
      let newPosition = this.state.currentPosition;
      delete newPosition[sourceSquare];
      this.setState({ currentPosition: newPosition });
      return;
    }

    let newPosition = this.state.currentPosition;
    sourceSquare !== 'spare' && delete newPosition[sourceSquare];
    newPosition[targetSquare] = piece;

    this.setState(() => ({ currentPosition: newPosition }));
  };

  setTouchState = e => this.setState({ wasPieceTouched: e.isTrusted });

  render() {
    const {
      sparePieces,
      width,
      imageFormat,
      pieceSvgs,
      piecePngs,
      onDrop,
      orientation,
      showNotation,
      transitionDuration,
      whiteSquareStyle,
      blackSquareStyle,
      boardStyle,
      id,
      customizeSquare,
      highLightedSquares,
      onMouseOverSquare,
      onMouseOutSquare,
      onHoverSquareStyle,
      highLightedSquareStyleWhite,
      highLightedSquareStyleBlack
    } = this.props;
    const {
      currentPosition,
      sourceSquare,
      targetSquare,
      sourcePiece,
      waitForTransition,
      phantomPiece,
      beginAnimation,
      dropSquare,
      wasPieceTouched
    } = this.state;

    return (
      <ChessboardContext.Provider
        value={{
          ...this.props,
          ...{
            sourceSquare,
            targetSquare,
            sourcePiece,
            waitForTransition,
            phantomPiece,
            setPosition: this.setPosition,
            transitionDuration,
            beginAnimation,
            dropSquare,
            setAnimation: this.setAnimation,
            setTransition: this.setTransition,
            setTouchState: this.setTouchState
          }
        }}
      >
        <div>
          {sparePieces && <SparePiecesTop />}
          <Board
            width={width}
            setPosition={this.setPosition}
            onDrop={onDrop}
            currentPosition={currentPosition}
            orientation={orientation}
            showNotation={showNotation}
            whiteSquareStyle={whiteSquareStyle}
            blackSquareStyle={blackSquareStyle}
            boardStyle={boardStyle}
            customizeSquare={customizeSquare}
            highLightedSquares={highLightedSquares}
            onMouseOverSquare={onMouseOverSquare}
            onMouseOutSquare={onMouseOutSquare}
            onHoverSquareStyle={onHoverSquareStyle}
            highLightedSquareStyleWhite={highLightedSquareStyleWhite}
            highLightedSquareStyleBlack={highLightedSquareStyleBlack}
          />
          {sparePieces && <SparePiecesBottom />}
        </div>
        <CustomDragLayer
          width={width}
          imageFormat={imageFormat}
          pieceSvgs={pieceSvgs}
          piecePngs={piecePngs}
          id={id}
          wasPieceTouched={wasPieceTouched}
        />
      </ChessboardContext.Provider>
    );
  }
}

export default DragDropContext(MultiBackend(HTML5toTouch))(Chessboard);
