import React from 'react';
import PropTypes from 'prop-types';

PhantomPiece.propTypes = {
  width: PropTypes.number,
  phantomPieceValue: PropTypes.string,
  pieceSvgs: PropTypes.object,
  piecePngs: PropTypes.string,
  imageFormat: PropTypes.string
};

function PhantomPiece({
  width,
  phantomPieceValue,
  pieceSvgs,
  piecePngs,
  imageFormat
}) {
  return imageFormat === 'png' ? (
    <img
      style={{
        position: 'absolute',
        width: width / 8,
        height: width / 8,
        zIndex: 0
      }}
      src={require(`${piecePngs}${phantomPieceValue}.png`)}
      alt=""
    />
  ) : (
    <svg
      viewBox={`-3 -3 50 50`}
      style={{
        position: 'absolute',
        width: width / 8,
        height: width / 8,
        zIndex: 0
      }}
    >
      {<g key={phantomPieceValue}>{pieceSvgs[phantomPieceValue]}</g>}
    </svg>
  );
}

export default PhantomPiece;
