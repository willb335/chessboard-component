import React, { Component } from 'react';
import PropTypes from 'prop-types';

const isActivePiece = (currentSquare, targetSquare) =>
  targetSquare && targetSquare === currentSquare;

const getTransitionCoordinates = ({
  getSquareCoordinates,
  sourceSq,
  targetSq
}) => {
  const transitionCoordinates = getSquareCoordinates(sourceSq, targetSq);
  const { sourceSquare, targetSquare } = transitionCoordinates;

  return `translate(${sourceSquare.x - targetSquare.x}px, ${sourceSquare.y -
    targetSquare.y}px)`;
};

class Png extends Component {
  static propTypes = {
    width: PropTypes.number,
    piece: PropTypes.string,
    isDragging: PropTypes.bool,
    currentSquare: PropTypes.string,
    getSquareCoordinates: PropTypes.func,
    piecePngs: PropTypes.string,
    sourceSquare: PropTypes.string,
    targetSquare: PropTypes.string,
    previousPosition: PropTypes.object,
    waitForTransition: PropTypes.bool,
    transitionDuration: PropTypes.number
  };

  getTransitionCoordinates = () => {
    const {
      waitForTransition,
      currentSquare,
      targetSquare,
      sourceSquare,
      getSquareCoordinates
    } = this.props;

    return (
      waitForTransition &&
      isActivePiece(currentSquare, targetSquare) &&
      getTransitionCoordinates({
        getSquareCoordinates,
        sourceSq: sourceSquare,
        targetSq: targetSquare
      })
    );
  };

  render() {
    const {
      piece,
      width,
      isDragging,
      piecePngs,
      transitionDuration
    } = this.props;

    return (
      <img
        data-testid={piece}
        style={{
          width: width / 8,
          height: width / 8,
          position: 'absolute',
          opacity: isDragging ? 0 : 1,
          transform: this.getTransitionCoordinates(),
          transition: `transform ${transitionDuration}ms`,
          zIndex: 10
        }}
        src={require(`${piecePngs}${piece}.png`)}
        alt=""
      />
    );
  }
}

export default Png;
