import React from 'react';
import { render } from 'react-testing-library';
import 'jest-dom/extend-expect';

import Chessboard from '../index';

describe('when the image format is svg', () => {
  test('renders pieces when position is "start"', () => {
    const { getByTestId } = render(<Chessboard position="start" />);

    const whiteQueen = getByTestId('wQ');
    const blackKnight = getByTestId('bN');

    expect(whiteQueen).toBeInTheDOM();
    expect(blackKnight).toBeInTheDOM();
  });

  test('renders no pieces when position is not provided', () => {
    const { queryByTestId } = render(<Chessboard position="" />);

    const whiteQueen = queryByTestId('wQ');
    const blackKnight = queryByTestId('bN');

    expect(whiteQueen).not.toBeInTheDOM();
    expect(blackKnight).not.toBeInTheDOM();
  });

  test('renders an extra piece when sparePieces is true', () => {
    const { queryAllByTestId } = render(
      <Chessboard position="start" sparePieces={true} />
    );

    const whiteQueen = queryAllByTestId('wQ');
    const blackRook = queryAllByTestId('bR');
    const blackPawn = queryAllByTestId('bP');

    expect(whiteQueen).toHaveLength(2);
    expect(blackRook).toHaveLength(3);
    expect(blackPawn).toHaveLength(9);
  });

  test('renders correct pieces when given a position object', () => {
    const { queryAllByTestId } = render(
      <Chessboard
        position={{
          d6: 'bK',
          d4: 'wP',
          e4: 'wK'
        }}
      />
    );
    const whiteQueen = queryAllByTestId('wK');
    const blackRook = queryAllByTestId('bR');
    const blackKing = queryAllByTestId('bK');
    const blackBishop = queryAllByTestId('bB');

    expect(whiteQueen).toHaveLength(1);
    expect(blackRook).toHaveLength(0);
    expect(blackKing).toHaveLength(1);
    expect(blackBishop).toHaveLength(0);
  });
});

describe('when the image format is png', () => {
  test('renders pieces when position is "start"', () => {
    const { getByTestId } = render(
      <Chessboard position="start" imageFormat="png" />
    );

    const whiteQueen = getByTestId('wQ');
    const blackKnight = getByTestId('bN');

    expect(whiteQueen).toBeInTheDOM();
    expect(blackKnight).toBeInTheDOM();
  });

  test('renders no pieces when position is not provided', () => {
    const { queryByTestId } = render(
      <Chessboard position="" imageFormat="png" />
    );

    const whiteQueen = queryByTestId('wQ');
    const blackKnight = queryByTestId('bN');

    expect(whiteQueen).not.toBeInTheDOM();
    expect(blackKnight).not.toBeInTheDOM();
  });

  test('renders an extra piece when sparePieces is true', () => {
    const { queryAllByTestId } = render(
      <Chessboard position="start" sparePieces={true} imageFormat="png" />
    );

    const whiteQueen = queryAllByTestId('wQ');
    const blackRook = queryAllByTestId('bR');
    const blackPawn = queryAllByTestId('bP');

    expect(whiteQueen).toHaveLength(2);
    expect(blackRook).toHaveLength(3);
    expect(blackPawn).toHaveLength(9);
  });

  test('renders correct pieces when given a position object', () => {
    const { queryAllByTestId } = render(
      <Chessboard
        position={{
          d6: 'bK',
          d4: 'wP',
          e4: 'wK'
        }}
        imageFormat="png"
      />
    );
    const whiteQueen = queryAllByTestId('wK');
    const blackRook = queryAllByTestId('bR');
    const blackKing = queryAllByTestId('bK');
    const blackBishop = queryAllByTestId('bB');

    expect(whiteQueen).toHaveLength(1);
    expect(blackRook).toHaveLength(0);
    expect(blackKing).toHaveLength(1);
    expect(blackBishop).toHaveLength(0);
  });
});
