import React from 'react';
import { render } from 'react-testing-library';
import Chessboard from '../index';
import 'jest-dom/extend-expect';

test('renders spare pieces when sparePieces prop is true', () => {
  const { getByTestId } = render(<Chessboard sparePieces={true} />);
  const sparePiecesTop = getByTestId('spare-top');
  const sparePiecesBottom = getByTestId('spare-bottom');

  expect(sparePiecesTop).toBeVisible();
  expect(sparePiecesBottom).toBeVisible();
});

test('renders a specific piece', () => {
  const { getByTestId } = render(<Chessboard sparePieces={true} />);
  const whiteQueen = getByTestId('spare-wQ');
  expect(whiteQueen).toBeVisible();
});
