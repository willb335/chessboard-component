import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'jest-dom/extend-expect';

import Chessboard from '../index';

afterEach(cleanup);

test('checks if the square has a style class and is in the DOM', () => {
  const { getByTestId } = render(
    <Chessboard whiteSquareStyle={{ backgroundColor: 'green' }} />
  );

  const whiteSquare = getByTestId('white-square');

  expect(whiteSquare).toHaveAttribute('style');
  expect(whiteSquare).toHaveClass('square');
  expect(whiteSquare).toBeInTheDOM();
});
