import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';

const layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 100,
  left: 0,
  top: 0
};

const getItemStyles = props => {
  const { currentOffset, wasPieceTouched } = props;

  if (!currentOffset) return { display: 'none' };

  let { x, y } = currentOffset;
  const transform = wasPieceTouched
    ? `translate(${x}px, ${y + -25}px) scale(2)`
    : `translate(${x}px, ${y}px)`;

  return { transform };
};

class CustomDragLayer extends Component {
  static propTypes = {
    item: PropTypes.object,
    currentOffset: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }),
    isDragging: PropTypes.bool.isRequired,
    width: PropTypes.number,
    imageFormat: PropTypes.string,
    pieceSvgs: PropTypes.object,
    piecePngs: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasPieceTouched: PropTypes.bool
  };

  render() {
    const {
      isDragging,
      width,
      item,
      imageFormat,
      pieceSvgs,
      piecePngs,
      id
    } = this.props;

    return isDragging && item.board === id ? (
      <div style={layerStyles}>
        <div style={getItemStyles(this.props)}>
          {imageFormat === 'png' ? (
            <img
              style={{ width: width / 8, height: width / 8 }}
              src={require(`${piecePngs}${item.piece}.png`)}
              alt=""
            />
          ) : (
            <svg
              viewBox={`-3 -3 50 50`}
              style={{ width: width / 8, height: width / 8 }}
            >
              {Object.keys(pieceSvgs).map(key => {
                if (key !== item.piece) return null;
                return <g key={key}>{pieceSvgs[key]}</g>;
              })}
            </svg>
          )}
        </div>
      </div>
    ) : null;
  }
}

function collect(monitor) {
  return {
    item: monitor.getItem(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging()
  };
}

export default DragLayer(collect)(CustomDragLayer);
