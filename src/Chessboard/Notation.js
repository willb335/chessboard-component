import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

Notation.propTypes = {
  row: PropTypes.number,
  col: PropTypes.number,
  alpha: PropTypes.array,
  orientation: PropTypes.string,
  width: PropTypes.number,
  whiteSquareStyle: PropTypes.object,
  blackSquareStyle: PropTypes.object
};

const getRow = (orientation, row) =>
  orientation === 'white' ? row + 1 : row - 1;
const getColumn = (orientation, alpha, col) => {
  return orientation === 'black' ? alpha[7 - col] : alpha[col];
};

function Notation({
  row,
  col,
  alpha,
  orientation,
  width,
  whiteSquareStyle,
  blackSquareStyle
}) {
  const whiteColor = whiteSquareStyle.backgroundColor;
  const blackColor = blackSquareStyle.backgroundColor;

  const isBottomLeftSquare =
    col === 0 &&
    ((orientation === 'white' && row === 0) ||
      (orientation === 'black' && row === 9));
  const isRow = col === 0;
  const isColumn =
    (orientation === 'white' && row === 0) ||
    (orientation === 'black' && row === 9);

  if (isBottomLeftSquare) {
    return (
      <Fragment>
        <div
          data-testid={`bottom-left-${getRow(orientation, row)}`}
          className={cx('notation')}
          style={{
            ...{ fontSize: width / 48, color: whiteColor },
            ...numericStyle(width)
          }}
        >
          {getRow(orientation, row)}
        </div>
        <div
          data-testid={`bottom-left-${getColumn(orientation, alpha, col)}`}
          className={cx('notation')}
          style={{
            ...{ fontSize: width / 48, color: whiteColor },
            ...alphaStyle(width)
          }}
        >
          {getColumn(orientation, alpha, col)}
        </div>
      </Fragment>
    );
  }

  if (isRow) {
    return (
      <div
        className={cx('notation')}
        style={{
          ...rowStyle({
            row,
            width,
            blackColor,
            whiteColor,
            orientation,
            isBottomLeftSquare,
            isRow
          }),
          ...numericStyle(width)
        }}
      >
        {getRow(orientation, row)}
      </div>
    );
  }

  if (isColumn) {
    return (
      <div
        data-testid={`column-${getColumn(orientation, alpha, col)}`}
        className={cx('notation')}
        style={{
          ...columnStyle({ col, width, blackColor, whiteColor }),
          ...alphaStyle(width)
        }}
      >
        {getColumn(orientation, alpha, col)}
      </div>
    );
  }
  return null;
}

export default Notation;

const columnStyle = ({ col, width, blackColor, whiteColor }) => ({
  fontSize: width / 48,
  color: col % 2 !== 0 ? blackColor : whiteColor
});

const rowStyle = ({
  row,
  width,
  blackColor,
  whiteColor,
  orientation,
  isBottomLeftSquare,
  isRow
}) => {
  return {
    fontSize: width / 48,
    color:
      orientation === 'black'
        ? isRow && !isBottomLeftSquare && row % 2 === 0
          ? blackColor
          : whiteColor
        : isRow && !isBottomLeftSquare && row % 2 !== 0
          ? blackColor
          : whiteColor
  };
};

const alphaStyle = width => ({
  position: 'absolute',
  bottom: width / 8 / 40,
  right: width / 8 / 20
});

const numericStyle = width => ({
  position: 'absolute',
  top: width / 8 / 40,
  left: width / 8 / 40
});
