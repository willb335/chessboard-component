import React from 'react';
import cx from 'classnames';

import Piece from './Piece';
import Chessboard from './index';

const buildSparePieces = ({
  imageFormat,
  dropOffBoard,
  draggable,
  sourceSquare,
  targetSquare,
  sourcePiece,
  animationOnDrop,
  pieceSvgs,
  width,
  orientation,
  setPosition,
  onDrop,
  piecePngs,
  beginAnimation,
  dropSquare,
  id,
  setAnimation,
  setTransition
}) => {
  const pieces =
    orientation === 'black'
      ? ['bK', 'bQ', 'bR', 'bB', 'bN', 'bP']
      : ['wK', 'wQ', 'wR', 'wB', 'wN', 'wP'];

  return pieces.map((p, i) => (
    <div
      key={i.toString()}
      data-testid={`spare-${p}`}
      style={{ width: width / 8, height: width / 8 }}
    >
      <Piece
        piece={p}
        width={width}
        imageFormat={imageFormat}
        setPosition={setPosition}
        pieceSvgs={pieceSvgs}
        piecePngs={piecePngs}
        currentSquare={'spare'}
        dropOffBoard={dropOffBoard}
        draggable={true}
        onDrop={onDrop}
        sourceSquare={sourceSquare}
        targetSquare={targetSquare}
        sourcePiece={sourcePiece}
        animationOnDrop={animationOnDrop}
        orientation={orientation}
        beginAnimation={beginAnimation}
        dropSquare={dropSquare}
        id={id}
        setAnimation={setAnimation}
        setTransition={setTransition}
      />
    </div>
  ));
};

export const SparePiecesTop = () => (
  <Chessboard.Consumer>
    {({
      imageFormat,
      dropOffBoard,
      draggable,
      sourceSquare,
      targetSquare,
      sourcePiece,
      animationOnDrop,
      pieceSvgs,
      width,
      orientation,
      setPosition,
      onDrop,
      piecePngs,
      beginAnimation,
      dropSquare,
      id,
      setAnimation,
      setTransition
    }) => (
      <div data-testid="spare-top" className={cx('sparePieces')}>
        {buildSparePieces({
          orientation: orientation === 'white' ? 'black' : 'white',
          imageFormat,
          dropOffBoard,
          draggable,
          sourceSquare,
          targetSquare,
          sourcePiece,
          animationOnDrop,
          pieceSvgs,
          width,
          setPosition,
          onDrop,
          piecePngs,
          beginAnimation,
          dropSquare,
          id,
          setAnimation,
          setTransition
        })}
      </div>
    )}
  </Chessboard.Consumer>
);

export const SparePiecesBottom = () => (
  <Chessboard.Consumer>
    {({
      imageFormat,
      dropOffBoard,
      draggable,
      sourceSquare,
      targetSquare,
      sourcePiece,
      animationOnDrop,
      pieceSvgs,
      width,
      orientation,
      setPosition,
      onDrop,
      piecePngs,
      beginAnimation,
      dropSquare,
      id,
      setAnimation,
      setTransition
    }) => (
      <div data-testid="spare-bottom" className={cx('sparePieces')}>
        {buildSparePieces({
          orientation: orientation === 'white' ? 'white' : 'black',
          imageFormat,
          dropOffBoard,
          draggable,
          sourceSquare,
          targetSquare,
          sourcePiece,
          animationOnDrop,
          pieceSvgs,
          width,
          setPosition,
          onDrop,
          piecePngs,
          beginAnimation,
          dropSquare,
          id,
          setAnimation,
          setTransition
        })}
      </div>
    )}
  </Chessboard.Consumer>
);
